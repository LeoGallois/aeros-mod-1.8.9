package fr.azerks.command;

import fr.azerks.ModTest;
import fr.azerks.gui.GUI_ENUM;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;

public class CommandShop implements ICommand {

    private final List aliases;

    public CommandShop() {
        aliases = new ArrayList();
        aliases.add("shop");
    }

    @Override
    public String getCommandName() {
        return "shop";
    }

    @Override
    public String getCommandUsage(ICommandSender iCommandSender) {
        return null;
    }

    @Override
    public List<String> getCommandAliases() {
        return this.aliases;
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) throws CommandException {
        World world = sender.getEntityWorld();

        if(!world.isRemote){
            if (args.length == 0){
                EntityPlayer player = (EntityPlayer) sender;
                sender.addChatMessage(new ChatComponentText("" + player + "  " + player.getEntityWorld()));
                player.openGui(ModTest.instance, GUI_ENUM.SHOP.ordinal(), player.getEntityWorld(), player.getPosition().getX(), player.getPosition().getY(), player.getPosition().getZ());
            }
        }
    }

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender iCommandSender) {
        return true;
    }

    @Override
    public List<String> addTabCompletionOptions(ICommandSender iCommandSender, String[] strings, BlockPos blockPos) {
        return null;
    }

    @Override
    public boolean isUsernameIndex(String[] strings, int i) {
        return false;
    }

    @Override
    public int compareTo(ICommand o) {
        return 0;
    }
}
