package fr.azerks.command;

import fr.azerks.utils.Player;
import fr.azerks.utils.References;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CommandPlayerMoney implements ICommand {

    private final List aliases;

    public CommandPlayerMoney() {
        aliases = new ArrayList();
        aliases.add("money");
    }

    @Override
    public String getCommandName() {
        return "money";
    }

    @Override
    public String getCommandUsage(ICommandSender iCommandSender) {
        return null;
    }

    @Override
    public List<String> getCommandAliases() {
        return this.aliases;
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) throws CommandException {
        World world = sender.getEntityWorld();
        Player playerSender = null;
        // TODO: 26/09/2019 Verifier que le joueur existe
        if (!world.isRemote){
            if (args.length == 0){
                EntityPlayer player = (EntityPlayer) sender;
                playerSender = References.mongo.getPlayerFromDB(sender.getName());
                System.out.println("Money : " + playerSender.getMoney() + "of :" + playerSender.getUsername() );
                player.addChatMessage(new ChatComponentText("money : " + playerSender.getMoney()));
            }else if (args[1].equalsIgnoreCase("pay")){
                if (args.length == 3){
                    Player playerReceiver = null;
                    playerReceiver = References.mongo.getPlayerFromDB(args[2]);
                    try {
                        References.mongo.payPlayer(playerSender, playerReceiver, Float.valueOf(args[3]));
                    }catch (NumberFormatException e){
                        sender.addChatMessage(new ChatComponentText("Merci de respecter la syntaxe suivante : /money pay <Joueur> <Montant>"));
                    }
                }
                sender.addChatMessage(new ChatComponentText("Merci de respecter la syntaxe suivante : /money pay <Joueur> <Montant>"));
            }
        }
    }

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender iCommandSender) {
        return true;
    }

    @Override
    public List<String> addTabCompletionOptions(ICommandSender iCommandSender, String[] strings, BlockPos blockPos) {
        return null;
    }

    @Override
    public boolean isUsernameIndex(String[] strings, int i) {
        return false;
    }

    @Override
    public int compareTo(ICommand o) {
        return 0;
    }
}
