package fr.azerks.armor;

import fr.azerks.init.ItemsInit;
import fr.azerks.utils.References;
import net.minecraft.entity.Entity;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ArmorCustom extends ItemArmor{

    private static final int[] maxDamageArray = new int[]{11, 16, 15, 13};
    public ArmorCustom material;

    public ArmorCustom(ItemArmor.ArmorMaterial material, int armorType) {
        super(material, 0 , armorType);
    }

    public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type){
        if(stack.getItem() == ItemsInit.zylium_leggings){
            return References.MODID + ":textures/models/armor/zylium_layer_2.png";
        }
        else if(stack.getItem() == ItemsInit.zylium_helmet || stack.getItem() == ItemsInit.zylium_chestplate || stack.getItem() == ItemsInit.zylium_boots)
            return References.MODID + ":textures/models/armor/zylium_layer_1.png";

        else if(stack.getItem() == ItemsInit.xamos_leggings){
            return References.MODID + ":textures/models/armor/xamos_layer_2.png";
        }
        else if(stack.getItem() == ItemsInit.xamos_helmet || stack.getItem() == ItemsInit.xamos_chestplate || stack.getItem() == ItemsInit.xamos_boots)
            return References.MODID + ":textures/models/armor/xamos_layer_1.png";

        else if(stack.getItem() == ItemsInit.trinitium_leggings){
            return References.MODID + ":textures/models/armor/trinitium_layer_2.png";
        }
        else if(stack.getItem() == ItemsInit.trinitium_helmet || stack.getItem() == ItemsInit.trinitium_chestplate || stack.getItem() == ItemsInit.trinitium_boots)
            return References.MODID + ":textures/models/armor/trinitium_layer_1.png";

        else if(stack.getItem() == ItemsInit.aeronium_leggings){
            return References.MODID + ":textures/models/armor/aeronium_layer_2.png";
        }
        else if(stack.getItem() == ItemsInit.aeronium_helmet || stack.getItem() == ItemsInit.aeronium_chestplate || stack.getItem() == ItemsInit.aeronium_boots)
            return References.MODID + ":textures/models/armor/aeronium_layer_1.png";
        else return null;
    }

    public boolean getIsRepairable(ItemStack input, ItemStack repair) {
        if(input.getItem() == ItemsInit.zylium_helmet ||
                input.getItem() == ItemsInit.zylium_chestplate ||
                input.getItem() == ItemsInit.zylium_leggings ||
                input.getItem() == ItemsInit.zylium_boots ){
            if(repair.getItem() == ItemsInit.zylium){
                return true;
            }else return false;
        }

        else if (input.getItem() == ItemsInit.xamos_helmet ||
                input.getItem() == ItemsInit.xamos_chestplate ||
                input.getItem() == ItemsInit.xamos_leggings ||
                input.getItem() == ItemsInit.xamos_boots ) {
            if (repair.getItem() == ItemsInit.xamos) {
                return true;
            }else return false;
        }

        else if(input.getItem() == ItemsInit.trinitium_helmet ||
                input.getItem() == ItemsInit.trinitium_chestplate ||
                input.getItem() == ItemsInit.trinitium_leggings ||
                input.getItem() == ItemsInit.trinitium_boots ){
            if (repair.getItem() == ItemsInit.trinitium) {
                return true;
            }else return false;
        }

        else if (input.getItem() == ItemsInit.aeronium_helmet ||
                input.getItem() == ItemsInit.aeronium_chestplate ||
                input.getItem() == ItemsInit.aeronium_leggings ||
                input.getItem() == ItemsInit.aeronium_boots){
            if (repair.getItem() == ItemsInit.aeronium) {
                return true;
            }else return false;
        }
        return false;
    }
}
