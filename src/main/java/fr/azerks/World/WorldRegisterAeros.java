package fr.azerks.World;

import net.minecraftforge.fml.common.IWorldGenerator;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class WorldRegisterAeros {
    public static void MainRegistry(){
        registerWorldGen(new WorldGenAeros(), -1);
        registerWorldGen(new WorldGenAeros(), 0);
        registerWorldGen(new WorldGenAeros(), 1);
    }

    private static void registerWorldGen(IWorldGenerator iWorldGenerator, int weightProbability){
        GameRegistry.registerWorldGenerator(iWorldGenerator, weightProbability);
    }
}
