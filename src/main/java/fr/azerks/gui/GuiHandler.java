package fr.azerks.gui;

import fr.azerks.gui.Container.ContainerShop;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler {
    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        TileEntity tileEntity = world.getTileEntity(new BlockPos(x, y, z));

        if (tileEntity != null)
        {
        }
        if (ID == GUI_ENUM.SHOP.ordinal())
        {
            return new ContainerShop(player.inventory, world, x, y, z);
        } if (ID == GUI_ENUM.KARMA.ordinal())
        {
            //return new ContainerDeconstructor(player.inventory, world, x, y, z);
        } if (ID == GUI_ENUM.COMMUNAL_BUILDINGS.ordinal())
        {
            //return new ContainerDeconstructor(player.inventory, world, x, y, z);
        }

        return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        TileEntity tileEntity = world.getTileEntity(new BlockPos(x, y, z));
        if (tileEntity != null){
        }
        if (ID == GUI_ENUM.SHOP.ordinal()){
            return new GuiShop(player.inventory, world, x, y, z);
        }
        return null;
    }
}
