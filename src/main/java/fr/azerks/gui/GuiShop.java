package fr.azerks.gui;

import fr.azerks.gui.Container.ContainerShop;
import fr.azerks.utils.Player;
import fr.azerks.utils.References;
import fr.azerks.utils.SQLController;
import fr.azerks.utils.ShopPrices;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import java.io.IOException;
import java.sql.SQLException;

@SideOnly(Side.CLIENT)
public class GuiShop extends GuiContainer {

    private InventoryPlayer playerInventory;
    private int bookImageWidth = 440;
    private int bookImageHeight = 220;

    private GuiButton btn_buy;
    private GuiButton btn_sell;
    private GuiButton btn_sellAll;
    // TODO: 24/09/2019 Quantity Start write to the end of the field
    private GuiTextField quantityField;
    protected int quantity;
    private Slot slotSelect = ContainerShop.getInvSlots().get(0);
    SQLController sqlConnection = References.sqlConnection;

    public GuiShop(InventoryPlayer inventory, World world, int x, int y, int z) {
        super(new ContainerShop(inventory, world, x, y, z));
        ShopPrices.registerItemsPrice();
        this.playerInventory = inventory;
    }

    @Override
    public void initGui() {
        super.initGui();
        buttonList.clear();
        Keyboard.enableRepeatEvents(true);
        this.buttonList.add(this.btn_buy = new GuiButton(0, 312,  143, 50, 20, "Buy"));
        this.buttonList.add(this.btn_buy = new GuiButton(1, 312,  164, 50, 20, "Sell"));
        this.buttonList.add(this.btn_buy = new GuiButton(2, 335,  185, 80, 20, "Sell all"));

        for (GuiButton button : buttonList){
            button.visible = true;
            button.enabled = true;
        }

        this.quantityField = new GuiTextField(0, this.fontRendererObj, width / 2 + 126, 156, 70, 20);
        this.quantityField.setMaxStringLength(10);
        this.quantityField.setText("1");
        updateScreen();
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        ItemStack[] inventory = playerInventory.mainInventory;
        Player player = null;
        sqlConnection.connection();
        float price = 0;
        if (button.enabled){
            switch (button.id){
                case 0:
                    if (quantityField.getText().length() > 0){
                        try {
                            this.quantity = Integer.parseInt(this.quantityField.getText());
                        }catch (NumberFormatException e){
                            mc.thePlayer.sendChatMessage("Entrez une quantité valide");
                            sqlConnection.disconnect();
                            break;
                        }
                    }
                    System.out.println("SHOP SIZE" + ShopPrices.getItemInShops().size());
                    if (slotSelect != null){
                        price = ShopPrices.getItemInShopsByID(slotSelect.slotNumber).getBuyPrice() * quantity;
                    }else {
                        this.mc.thePlayer.sendChatMessage("Please select a valide slot");
                        sqlConnection.disconnect();
                        break;
                    }
                    try {
                        player = sqlConnection.getPlayer(mc.thePlayer.getName());
                    } catch (SQLException e) {
                        e.printStackTrace();
                        sqlConnection.disconnect();
                        break;
                    }

                    if (player.getMoney() >= price){
                        References.sqlConnection.update(-price);
                        playerInventory.setInventorySlotContents(playerInventory.getFirstEmptyStack(), slotSelect.getStack());
                    }
                    sqlConnection.disconnect();
                    break;
                case 1:
                    break;
                case 2:
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {

        // Check if the input is a number && backspace
        // KeyCode LWJGL
        if (typedChar == '0' || typedChar == '1' || typedChar == '2' || typedChar == '3' || typedChar == '4' || typedChar == '5'||
                typedChar == '6' || typedChar == '7' || typedChar == '8' || typedChar == '9' || keyCode == 14) {
            this.quantityField.textboxKeyTyped(typedChar, keyCode);
        }

        if (!quantityField.isFocused()){
            super.keyTyped(typedChar, keyCode);
        }
        else{
            if (keyCode == 1) this.mc.thePlayer.closeScreen();
        }
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        this.quantityField.mouseClicked(mouseX, mouseY, mouseButton);

        Slot slot = getSlotUnderMouse();
        if (slot != null){
            slotSelect = slot;
            ContainerShop.getInvSlots().get(12).inventory.setInventorySlotContents(12, slot.getStack());
        }
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        this.quantityField.updateCursorCounter();
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {

        int offsetFromScreenLeft = (width - (bookImageWidth - 120)) / 2;
        /** Text, x, y , color */
        this.fontRendererObj.drawString("Shop", offsetFromScreenLeft, -20, 1);

    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float v, int i, int i1) {

        GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        GlStateManager.disableDepth();

        int offsetFromScreenLeft = (width - (bookImageWidth)) / 2;
        int offsetFromScreenTop = (height - (bookImageHeight + 20)) / 2;

        mc.renderEngine.bindTexture(new ResourceLocation("modtest:textures/gui/shop.png"));
        drawModalRectWithCustomSizedTexture(offsetFromScreenLeft, offsetFromScreenTop, 0, 0, bookImageWidth, bookImageHeight, bookImageWidth, bookImageHeight);
        this.quantityField.drawTextBox();
    }

    @Override
    public void onGuiClosed() {
        Keyboard.enableRepeatEvents(false);
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }

    @Override
    public Slot getSlotUnderMouse() {
        return super.getSlotUnderMouse();
    }



    @SideOnly(Side.CLIENT)
    static class ShopButton extends GuiButton{


        private int widthX;
        private int heightX;
        private String textureName;

        public ShopButton(int buttonID, int posx, int posy, int width, int height, String textureName) {
            super(buttonID, posx, posy, "");
            this.textureName = textureName;
            this.widthX = width;
            this.heightX = height;
        }

        @Override
        public void drawButton(Minecraft mc, int mouseX, int mouseY) {
            boolean flag = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;

            GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            mc.renderEngine.bindTexture(new ResourceLocation("modtest:textures/gui/" + textureName + ".png"));
            this.drawTexturedModalRect(this.xPosition, this.yPosition, widthX, heightX, this.width, this.height);
        }
    }
}
