package fr.azerks.gui.Container;

import com.google.common.collect.Lists;
import fr.azerks.gui.Inventory.InventoryShop;
import fr.azerks.init.ItemsInit;
import fr.azerks.utils.ShopPrices;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;

public class ContainerShop extends Container {

    private static InventoryShop inventoryShop = new InventoryShop();
    private World world;
    private int x = 0;
    private int y = 0;
    private int z = 0;
    private InventoryPlayer inventoryPlayer;
    private static List<Slot> invSlots = Lists.newArrayList();
    public ContainerShop(InventoryPlayer inventoryPlayer, World parWorld, int parX, int parY, int parZ) {
        this.x = parX;
        this.y = parY;
        this.z = parZ;
        this.world = parWorld;
        this.inventoryPlayer =  inventoryPlayer;

        int slotIndex = 0;
        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 3; j++) {
                addSlotToContainer(new Slot(this.inventoryShop, slotIndex, -65+ j * 76, 15 + i * 35 ));
                slotIndex++;
            }
        }

        //inventoryShop.setInventorySlotContents(0, new ItemStack(ItemsInit.aeronium, 1));
        inventorySlots.get(1).inventory.setInventorySlotContents(0, new ItemStack(ItemsInit.xamos, 64));
        inventoryShop.setInventorySlotContents(1, new ItemStack(ItemsInit.xamos, 1));
        inventoryShop.setInventorySlotContents(2, new ItemStack(ItemsInit.zylium, 1));
        inventoryShop.setInventorySlotContents(3, new ItemStack(ItemsInit.trinitium, 1));
        inventoryShop.setInventorySlotContents(4, new ItemStack(ItemsInit.aeronium_axe, 1));
        inventoryShop.setInventorySlotContents(5, new ItemStack(ItemsInit.trinitium_hoe, 1));
        inventoryShop.setInventorySlotContents(6, new ItemStack(ItemsInit.aeronium_sword, 1));
        inventoryShop.setInventorySlotContents(7, new ItemStack(ItemsInit.xamos_boots, 1));
        inventoryShop.setInventorySlotContents(8, new ItemStack(ItemsInit.xamos_leggings, 1));
        inventoryShop.setInventorySlotContents(9, new ItemStack(ItemsInit.xamos_chestplate, 1));
        inventoryShop.setInventorySlotContents(10, new ItemStack(ItemsInit.xamos_helmet, 1));
        inventoryShop.setInventorySlotContents(11, new ItemStack(ItemsInit.ocb_paper, 1));

        addSlotToContainer(new Slot(this.inventoryShop, 12, 213, 75));
        inventoryShop.setInventorySlotContents(12, new ItemStack(ItemsInit.zylium_shovel, 1));

        invSlots.addAll(inventorySlots);
    }

    @Override
    public boolean canMergeSlot(ItemStack p_canMergeSlot_1_, Slot p_canMergeSlot_2_) {
        return false;
    }

    @Override
    public ItemStack slotClick(int p_slotClick_1_, int p_slotClick_2_, int p_slotClick_3_, EntityPlayer p_slotClick_4_) {
        return null;
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer p_transferStackInSlot_1_, int p_transferStackInSlot_2_) {
        return null;
    }

    @Override
    public boolean canInteractWith(EntityPlayer entityPlayer) {
        return true;
    }

    public static InventoryShop getInventoryShop() {
        return inventoryShop;
    }

    public static List<Slot> getInvSlots() {
        return invSlots;
    }
}
