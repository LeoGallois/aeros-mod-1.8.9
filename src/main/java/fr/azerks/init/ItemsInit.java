package fr.azerks.init;

import fr.azerks.armor.ArmorCustom;
import fr.azerks.tools.ItemAxeCustom;
import fr.azerks.tools.ItemPickaxeCustom;
import fr.azerks.utils.References;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.*;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemsInit {

    public static Item zylium_axe, zylium_sword, zylium_shovel,zylium_pickaxe, zylium_hoe;
    public static Item xamos_sword, xamos_axe, xamos_pickaxe, xamos_shovel, xamos_hoe;
    public static Item trinitium_sword, trinitium_axe, trinitium_pickaxe, trinitium_shovel, trinitium_hoe;
    public static Item aeronium_sword, aeronium_axe, aeronium_pickaxe, aeronium_shovel, aeronium_hoe;

    public static Item ocb_paper;
    public static Item zylium, xamos, trinitium, aeronium;
    public static Item zylium_helmet, zylium_chestplate, zylium_leggings, zylium_boots;
    public static Item xamos_helmet, xamos_chestplate, xamos_leggings, xamos_boots;
    public static Item trinitium_helmet, trinitium_chestplate, trinitium_leggings, trinitium_boots;
    public static Item aeronium_helmet, aeronium_chestplate, aeronium_leggings, aeronium_boots;

    public static ItemArmor.ArmorMaterial zyliumMaterial = EnumHelper.addArmorMaterial("armorZylium", "zylium",25, new int[]{4 ,5 ,6 ,3}, 20);
    public static ItemArmor.ArmorMaterial xamosMaterial = EnumHelper.addArmorMaterial("armorXamos", "xamos",25, new int[]{4 ,5 ,6 ,3}, 20);
    public static ItemArmor.ArmorMaterial trinitiumMaterial = EnumHelper.addArmorMaterial("armorTrinitium", "trinitium",25, new int[]{4 ,5 ,6 ,3}, 20);
    public static ItemArmor.ArmorMaterial aeroniumMaterial = EnumHelper.addArmorMaterial("armorAeronium", "aeronium",25, new int[]{4 ,5 ,6 ,3}, 20);

    public static Item.ToolMaterial ZYLIUM =  EnumHelper.addToolMaterial("ZYLIUM", 3, 3, 4, 5, 20);
    public static Item.ToolMaterial XAMOS =  EnumHelper.addToolMaterial("XAMOS", 3, 3, 4, 5, 20);
    public static Item.ToolMaterial TRINITIUM =  EnumHelper.addToolMaterial("TRINITIUM", 3, 3, 4, 5, 20);
    public static Item.ToolMaterial AERONIUM =  EnumHelper.addToolMaterial("AERONIUM", 3, 3, 4, 5, 20);

    public static void init(){
        ocb_paper = new Item().setRegistryName("ocb_paper").setUnlocalizedName("ocb_paper").setCreativeTab(CreativeTabs.tabMaterials);
        zylium = new Item().setRegistryName("zylium").setUnlocalizedName("zylium").setCreativeTab(CreativeTabs.tabMaterials);
        xamos = new Item().setRegistryName("xamos").setUnlocalizedName("xamos").setCreativeTab(CreativeTabs.tabMaterials);
        trinitium = new Item().setRegistryName("trinitium").setUnlocalizedName("trinitium").setCreativeTab(CreativeTabs.tabMaterials);
        aeronium = new Item().setRegistryName("aeronium").setUnlocalizedName("aeronium").setCreativeTab(CreativeTabs.tabMaterials);

        zylium_helmet = new ArmorCustom(zyliumMaterial, 0).setRegistryName("ZyliumHelmet").setUnlocalizedName("ZyliumHelmet").setCreativeTab(CreativeTabs.tabCombat);
        zylium_chestplate = new ArmorCustom(zyliumMaterial, 1).setRegistryName("ZyliumChestplate").setUnlocalizedName("ZyliumChestplate").setCreativeTab(CreativeTabs.tabCombat);
        zylium_leggings = new ArmorCustom(zyliumMaterial, 2).setRegistryName("ZyliumLeggings").setUnlocalizedName("ZyliumLeggings").setCreativeTab(CreativeTabs.tabCombat);
        zylium_boots = new ArmorCustom(zyliumMaterial, 3).setRegistryName("ZyliumBoots").setUnlocalizedName("ZyliumBoots").setCreativeTab(CreativeTabs.tabCombat);

        xamos_helmet = new ArmorCustom(xamosMaterial, 0).setRegistryName("XamosHelmet").setUnlocalizedName("XamosHelmet").setCreativeTab(CreativeTabs.tabCombat);
        xamos_chestplate = new ArmorCustom(xamosMaterial, 1).setRegistryName("XamosChestplate").setUnlocalizedName("XamosChestplate").setCreativeTab(CreativeTabs.tabCombat);
        xamos_leggings = new ArmorCustom(xamosMaterial, 2).setRegistryName("XamosLeggings").setUnlocalizedName("XamosLeggings").setCreativeTab(CreativeTabs.tabCombat);
        xamos_boots = new ArmorCustom(xamosMaterial, 3).setRegistryName("XamosBoots").setUnlocalizedName("XamosBoots").setCreativeTab(CreativeTabs.tabCombat);

        trinitium_helmet = new ArmorCustom(trinitiumMaterial, 0).setRegistryName("TrinitiumHelmet").setUnlocalizedName("TrinitiumHelmet").setCreativeTab(CreativeTabs.tabCombat);
        trinitium_chestplate = new ArmorCustom(trinitiumMaterial, 1).setRegistryName("TrinitiumChestplate").setUnlocalizedName("TrinitiumChestplate").setCreativeTab(CreativeTabs.tabCombat);
        trinitium_leggings = new ArmorCustom(trinitiumMaterial, 2).setRegistryName("TrinitiumLeggings").setUnlocalizedName("TrinitiumLeggings").setCreativeTab(CreativeTabs.tabCombat);
        trinitium_boots = new ArmorCustom(trinitiumMaterial, 3).setRegistryName("TrinitiumBoots").setUnlocalizedName("TrinitiumBoots").setCreativeTab(CreativeTabs.tabCombat);

        aeronium_helmet = new ArmorCustom(aeroniumMaterial, 0).setRegistryName("AeroniumHelmet").setUnlocalizedName("AeroniumHelmet").setCreativeTab(CreativeTabs.tabCombat);
        aeronium_chestplate = new ArmorCustom(aeroniumMaterial, 1).setRegistryName("AeroniumChestplate").setUnlocalizedName("AeroniumChestplate").setCreativeTab(CreativeTabs.tabCombat);
        aeronium_leggings = new ArmorCustom(aeroniumMaterial, 2).setRegistryName("AeroniumLeggings").setUnlocalizedName("AeroniumLeggings").setCreativeTab(CreativeTabs.tabCombat);
        aeronium_boots = new ArmorCustom(aeroniumMaterial, 3).setRegistryName("AeroniumBoots").setUnlocalizedName("AeroniumBoots").setCreativeTab(CreativeTabs.tabCombat);

        zylium_sword = new ItemSword(ZYLIUM).setRegistryName("zylium_sword").setUnlocalizedName("zylium_sword");
        zylium_axe = new ItemAxeCustom(ZYLIUM).setRegistryName("zylium_axe").setUnlocalizedName("zylium_axe");
        zylium_pickaxe = new ItemPickaxeCustom(ZYLIUM).setRegistryName("zylium_pickaxe").setUnlocalizedName("zylium_pickaxe");
        zylium_shovel =new ItemSpade(ZYLIUM).setRegistryName("zylium_shovel").setUnlocalizedName("zylium_shovel");
        zylium_hoe = new ItemHoe(ZYLIUM).setRegistryName("zylium_hoe").setUnlocalizedName("zylium_hoe");

        xamos_sword =new ItemSword(XAMOS).setRegistryName("xamos_sword").setUnlocalizedName("xamos_sword");
        xamos_axe = new ItemAxeCustom(XAMOS).setRegistryName("xamos_axe").setUnlocalizedName("xamos_axe");
        xamos_pickaxe = new ItemPickaxeCustom(XAMOS).setRegistryName("xamos_pickaxe").setUnlocalizedName("xamos_pickaxe");
        xamos_shovel =new ItemSpade(XAMOS).setRegistryName("xamos_shovel").setUnlocalizedName("xamos_shovel");
        xamos_hoe = new ItemHoe(XAMOS).setRegistryName("xamos_hoe").setUnlocalizedName("xamos_hoe");

        trinitium_sword =new ItemSword(TRINITIUM).setRegistryName("trinitium_sword").setUnlocalizedName("trinitium_sword");
        trinitium_axe = new ItemAxeCustom(TRINITIUM).setRegistryName("trinitium_axe").setUnlocalizedName("trinitium_axe");
        trinitium_pickaxe = new ItemPickaxeCustom(TRINITIUM).setRegistryName("trinitium_pickaxe").setUnlocalizedName("trinitium_pickaxe");
        trinitium_shovel = new ItemSpade(TRINITIUM).setRegistryName("trinitium_shovel").setUnlocalizedName("trinitium_shovel");
        trinitium_hoe =new ItemHoe(TRINITIUM).setRegistryName("trinitium_hoe").setUnlocalizedName("trinitium_hoe");

        aeronium_sword = new ItemSword(AERONIUM).setRegistryName("aeronium_sword").setUnlocalizedName("aeronium_sword");
        aeronium_axe = new ItemAxeCustom(AERONIUM).setRegistryName("aeronium_axe").setUnlocalizedName("aeronium_axe");
        aeronium_pickaxe = new ItemPickaxeCustom(AERONIUM).setRegistryName("aeronium_pickaxe").setUnlocalizedName("aeronium_pickaxe");
        aeronium_shovel =new ItemSpade(AERONIUM).setRegistryName("aeronium_shovel").setUnlocalizedName("aeronium_shovel");
        aeronium_hoe = new ItemHoe(AERONIUM).setRegistryName("aeronium_hoe").setUnlocalizedName("aeronium_hoe");
    }

    public static void registerItems(){
        registerItem(ocb_paper);
        registerItem(zylium);
        registerItem(xamos);
        registerItem(trinitium);
        registerItem(aeronium);

        registerItem(zylium_helmet);
        registerItem(zylium_chestplate);
        registerItem(zylium_leggings);
        registerItem(zylium_boots);

        registerItem(xamos_helmet);
        registerItem(xamos_chestplate);
        registerItem(xamos_leggings);
        registerItem(xamos_boots);

        registerItem(trinitium_helmet);
        registerItem(trinitium_chestplate);
        registerItem(trinitium_leggings);
        registerItem(trinitium_boots);

        registerItem(aeronium_helmet);
        registerItem(aeronium_chestplate);
        registerItem(aeronium_leggings);
        registerItem(aeronium_boots);

        registerItem(zylium_sword);
        registerItem(zylium_axe);
        registerItem(zylium_pickaxe);
        registerItem(zylium_shovel);
        registerItem(zylium_hoe);

        registerItem(xamos_sword);
        registerItem(xamos_axe);
        registerItem(xamos_pickaxe);
        registerItem(xamos_shovel);
        registerItem(xamos_hoe);

        registerItem(trinitium_sword);
        registerItem(trinitium_axe);
        registerItem(trinitium_pickaxe);
        registerItem(trinitium_shovel);
        registerItem(trinitium_hoe);

        registerItem(aeronium_sword);
        registerItem(aeronium_axe);
        registerItem(aeronium_pickaxe);
        registerItem(aeronium_shovel);
        registerItem(aeronium_hoe);



        GameRegistry.addShapedRecipe(new ItemStack(ItemsInit.zylium_boots, 1), new Object[] {"   ", "# #", "# #", '#', ItemsInit.zylium});
    }

    @SideOnly(Side.CLIENT)
    public static void registerRenders(){
        registerRender(ocb_paper, 0);
        registerRender(zylium, 0);
        registerRender(xamos, 0);
        registerRender(trinitium, 0);
        registerRender(aeronium, 0);

        registerRenderArmor(zylium_helmet, 0);
        registerRenderArmor(zylium_chestplate, 0);
        registerRenderArmor(zylium_leggings, 0);
        registerRenderArmor(zylium_boots, 0);

        registerRenderArmor(xamos_helmet, 0);
        registerRenderArmor(xamos_chestplate, 0);
        registerRenderArmor(xamos_leggings, 0);
        registerRenderArmor(xamos_boots, 0);

        registerRenderArmor(trinitium_helmet, 0);
        registerRenderArmor(trinitium_chestplate, 0);
        registerRenderArmor(trinitium_leggings, 0);
        registerRenderArmor(trinitium_boots, 0);

        registerRenderArmor(aeronium_helmet, 0);
        registerRenderArmor(aeronium_chestplate, 0);
        registerRenderArmor(aeronium_leggings, 0);
        registerRenderArmor(aeronium_boots, 0);

        registerRenderTools(zylium_sword, 0);
        registerRenderTools(zylium_axe, 0);
        registerRenderTools(zylium_pickaxe, 0);
        registerRenderTools(zylium_shovel, 0);
        registerRenderTools(zylium_hoe, 0);

        registerRenderTools(xamos_sword, 0);
        registerRenderTools(xamos_axe, 0);
        registerRenderTools(xamos_pickaxe, 0);
        registerRenderTools(xamos_shovel, 0);
        registerRenderTools(xamos_hoe, 0);

        registerRenderTools(trinitium_sword, 0);
        registerRenderTools(trinitium_axe, 0);
        registerRenderTools(trinitium_pickaxe, 0);
        registerRenderTools(trinitium_shovel, 0);
        registerRenderTools(trinitium_hoe, 0);

        registerRenderTools(aeronium_sword, 0);
        registerRenderTools(aeronium_axe, 0);
        registerRenderTools(aeronium_pickaxe,0);
        registerRenderTools(aeronium_shovel, 0);
        registerRenderTools(aeronium_hoe, 0);
    }

    private static void registerItem(Item item)
    {
        GameRegistry.registerItem(item);
    }

    private static void registerRender(Item item,int meta)
    {
        ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(References.MODID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
    }

    private static void registerRenderArmor(Item item,int meta)
    {
        ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(References.MODID + ":armor/" + item.getUnlocalizedName().substring(5), "inventory"));
    }

    private static void registerRenderTools(Item item,int meta)
    {
        ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(References.MODID + ":itemTools/" + item.getUnlocalizedName().substring(5), "inventory"));
    }

}
