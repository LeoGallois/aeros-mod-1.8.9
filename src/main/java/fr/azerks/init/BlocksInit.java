package fr.azerks.init;

import fr.azerks.blocks.BlockShop;
import fr.azerks.utils.References;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlocksInit {

    public static Block zylium_ore, xamos_ore, trinitium_ore, aeronium_ore;
    public static Block shopBlock;

    public static void init(){
        zylium_ore = (new Block(Material.rock)).setHardness(2.0F).setResistance(10.0F).setRegistryName("zylium_ore").setUnlocalizedName("zylium_ore").setCreativeTab(CreativeTabs.tabMaterials);
        xamos_ore = (new Block(Material.rock)).setHardness(2.0F).setResistance(10.0F).setRegistryName("xamos_ore").setUnlocalizedName("xamos_ore").setCreativeTab(CreativeTabs.tabMaterials);
        trinitium_ore = (new Block(Material.rock)).setHardness(2.0F).setResistance(10.0F).setRegistryName("trinitium_ore").setUnlocalizedName("trinitium_ore").setCreativeTab(CreativeTabs.tabMaterials);
        aeronium_ore = (new Block(Material.rock)).setHardness(2.0F).setResistance(10.0F).setRegistryName("aeronium_ore").setUnlocalizedName("aeronium_ore").setCreativeTab(CreativeTabs.tabMaterials);
        shopBlock = (new BlockShop(Material.rock)).setHardness(2.0F).setResistance(10.0F).setRegistryName("shop").setUnlocalizedName("shop").setCreativeTab(CreativeTabs.tabMaterials);
    }
    public static void registersBlocks(){
        register(zylium_ore);
        register(xamos_ore);
        register(trinitium_ore);
        register(aeronium_ore);

        register(shopBlock);
    }

    public static void registerRenders(){
        registerRender(zylium_ore, 0);
        registerRender(xamos_ore, 0);
        registerRender(trinitium_ore, 0);
        registerRender(aeronium_ore, 0);

        registerRender(shopBlock, 0);

    }

    private static void register(Block block){
        GameRegistry.registerBlock(block, block.getUnlocalizedName().substring(5));
    }

    private static void registerRender(Block block, int meta){
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(Item.getItemFromBlock(block), meta, new ModelResourceLocation(References.MODID  + ":" + block.getUnlocalizedName().substring(5)));
    }
}
