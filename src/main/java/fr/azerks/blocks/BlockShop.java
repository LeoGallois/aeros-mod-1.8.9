package fr.azerks.blocks;

import fr.azerks.ModTest;
import fr.azerks.gui.GUI_ENUM;
import fr.azerks.utils.ShopPrices;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;

public class BlockShop extends Block {
    public BlockShop(Material rock) {
        super(rock);
    }

    @Override
    public boolean onBlockActivated(World parWorld, BlockPos parBlockPos, IBlockState parIBlockState, EntityPlayer parPlayer, EnumFacing parSide, float hitX, float hitY, float hitZ) {
        if(parWorld.isRemote){
            return true;
        }
        else {
            System.out.println("Block Shop");
            parPlayer.openGui(ModTest.instance, GUI_ENUM.SHOP.ordinal(), parWorld, parBlockPos.getX(), parBlockPos.getY(), parBlockPos.getZ());
            return true;
        }
    }
}
