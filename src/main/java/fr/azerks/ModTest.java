package fr.azerks;

import fr.azerks.command.CommandPlayerMoney;
import fr.azerks.command.CommandShop;
import fr.azerks.proxy.CommonProxy;
import fr.azerks.utils.MongoDBController;
import fr.azerks.utils.Player;
import fr.azerks.utils.References;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.*;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.apache.logging.log4j.Logger;

import java.sql.Ref;
import java.sql.SQLException;
import java.util.UUID;

@Mod(modid = References.MODID, version = References.VERSION, acceptedMinecraftVersions = References.MCVERSION)
public class ModTest
{


    public static Logger logger;
    public static Minecraft mc;


    @Mod.Instance(References.MODID)
    public static ModTest instance;

    @SidedProxy(clientSide = References.CLIENT_PROXY, serverSide = References.SERVER_PROXY, modId = References.MODID)
    public static CommonProxy proxy;

    @EventHandler
    public void onServerStart(FMLServerStartingEvent event){

    }

    @EventHandler
    public void preInit(FMLPreInitializationEvent event){
        logger = event.getModLog();
        proxy.preInit(event.getSuggestedConfigurationFile());
    }

    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        proxy.init();
        //NetworkRegistry.INSTANCE.registerGuiHandler(instance, new GuiHandlerAeros);
    }

    @EventHandler
    public void serverLoad(FMLServerStartingEvent event){
        event.registerServerCommand(new CommandShop());
        event.registerServerCommand(new CommandPlayerMoney());

        References.mongo.connection();
        References.mongo.createPlayer(this.mc.thePlayer.getUniqueID(), this.mc.thePlayer.getName(), "leogallois1301@gmail.com", "L14041997", 500000);
    }

    @EventHandler
    public void serverClose(FMLServerStoppedEvent event){

    }

    /*
    @EventHandler
    public void onPlayerConnection(PlayerEvent.PlayerLoggedInEvent event){
        EntityPlayer player = event.player;
        Player p = References.mongo.getPlayerFromDB("AzerKs");

        System.out.println("You are in onPlayerConnectionEvent");
        if (p.getUUID() == null || p.getUUID().toString().equalsIgnoreCase("")) {
            References.mongo.updatePlayer(p.getUsername(), p.getUUID());
        }
    }*/
}
