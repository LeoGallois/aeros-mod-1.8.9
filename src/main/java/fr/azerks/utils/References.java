package fr.azerks.utils;

import fr.azerks.init.ItemsInit;
import net.minecraft.init.Items;
import net.minecraft.item.Item;

public class References {

    public static final String MODID = "modtest";
    public static final String VERSION = "1.0";
    public static final String MCVERSION = "[1.8.9]";

    public static final String CLIENT_PROXY = "fr.azerks.proxy.ClientProxy";
    public static final String SERVER_PROXY = "fr.azerks.proxy.ServerProxy";

    public static final String URL_BASE = "jdbc:mysql://";
    public static final String HOST = "localhost";
    public static final String USERNAME = "root";
    public static final String PASSWORD = "";

    public static final String DATABASE_PLAYER = "player";
    public static final String DATABASE_FACTION = "faction";

    public static SQLController sqlConnection = new SQLController(References.URL_BASE, References.HOST, References.DATABASE_PLAYER, References.USERNAME, References.PASSWORD);

    public static MongoDBController mongo = new MongoDBController("", "", "localhost", 27017, "aeros");

    /** sudo systemct1 restart mongod.service // sudo ufw allow from IP to any port 27017 // mongo -u AdminAzerKs -p --authenticationDatabase admin */
}
