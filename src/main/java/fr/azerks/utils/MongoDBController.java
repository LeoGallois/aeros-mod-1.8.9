package fr.azerks.utils;

import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.UUID;

import static com.mongodb.client.model.Filters.eq;

public class MongoDBController {

    /**
     * https://mongodb.github.io/mongo-java-driver/3.0/driver/reference/connecting/authenticating/
     *  See x.509 for ssl Connection
     * */

    public static MongoClient mongoClient = null;
    private static MongoClientOptions options;
    private static MongoCredential credential;
    public static MongoDatabase db;
    public static MongoCollection<Document> playerCollection;
    public static MongoCollection<Document> factionCollection;
    private static int port;
    private static String host;

    public MongoDBController(String username, String pass, String host, int port, String dbName) {
        credential = MongoCredential.createCredential(username, dbName, pass.toCharArray());
        options = MongoClientOptions.builder().sslEnabled(false).build();
        this.host = host;
        this.port = port;
    }

    public static void connection(){
        if (!isConnected()) {
            mongoClient = new MongoClient(new ServerAddress(host , port)/*, credential, options*/);
            db = mongoClient.getDatabase("aeros");
            getPlayerCollection();
            getFactionCollection();
            System.out.println("You have been connected : " + getPlayerCollection());
        }
    }

    public static MongoCollection getPlayerCollection(){
        return playerCollection = db.getCollection("player");
    }

    public static MongoCollection getFactionCollection(){
        return factionCollection = db.getCollection("faction");
    }

    private static void disconnect(){
        mongoClient.close();
    }

    private static boolean isConnected() {
        return mongoClient != null;
    }

    // TODO: 26/09/2019 ADD DATE CREATION AND FACTION
    public static void createPlayer(UUID uuid, String username, String email, String password, int money){
        connection();
        if (!hasAccount(username)){
            Document doc = new Document("UUID", uuid.toString())
                    .append("username", username)
                    .append("email", email)
                    .append("password", password)
                    .append("money", money);
            playerCollection.insertOne(doc);
        }
    }

    public static void updatePlayer(String playerName, UUID uuid){
        connection();
        if (hasAccount(playerName)) {
            playerCollection.updateOne(eq("username", playerName), new Document("$set", new Document("UUID", uuid.toString())));
        }

    }

    public static void updatePlayer(String playerName, int money){
        connection();
        if (hasAccount(playerName)){
            playerCollection.updateOne(eq("username", playerName), new Document("$set", new Document("money", money)));
        }

    }

    public static void updatePlayer(String actualPlayerName, String newUsername){
        connection();
        if (hasAccount(actualPlayerName)){
            playerCollection.updateOne(eq("username", actualPlayerName), new Document("$set", new Document("username", newUsername)));
        }

    }

    public static void updatePlayer(String actualPlayername, UUID uuid, String username, String email, String password, int money){
        connection();
        if (hasAccount(actualPlayername)){
            playerCollection.updateOne(eq("username", actualPlayername), new Document("$set", new Document("UUID", uuid.toString())
                    .append("username", username)
                    .append("email", email)
                    .append("password", password)
                    .append("money", money)));
        }
    }

    /**
     * @param player1 sender
     * @param player2 receiver
     * @param amount amount of money pay
     */

    public static void payPlayer(Player player1, Player player2, Float amount){
        connection();
        // Sender
        playerCollection.updateOne(eq("usernmae", player1.getUsername()), new Document("$set", new Document("money", -amount)));

        // Receiver
        playerCollection.updateOne(eq("usernmae", player1.getUsername()), new Document("$set", new Document("money", +amount)));
    }

    public static boolean hasAccount(String username){
        connection();
        Document query = playerCollection.find(eq("username", username)).first();
        if (query != null){
            if (query.get("username").toString().equalsIgnoreCase(username)){
                return true;
            }
        }
        return false;
    }

    public static Player getPlayerFromDB(String playerUsername){
        connection();
        // TODO: 25/09/2019 return null be impossible to connect
        if (hasAccount(playerUsername)){
            connection();
            UUID uuid = null;
            String playerName = null;
            String email = null;
            String password = null;
            float money = 0;
            BasicDBObject searchQuery = new BasicDBObject();
            searchQuery.put("username", playerUsername);

            Document document = playerCollection.find(eq("username", playerUsername)).first();
            if (document != null || document.size() != 0){
                if (!document.get("UUID").toString().equalsIgnoreCase("")){
                    uuid = UUID.fromString(document.get("UUID").toString());
                }else{
                    uuid = null;
                }
                playerName = document.get("username").toString();
                email = document.get("email").toString();
                password = document.get("password").toString();
                money = Float.valueOf(document.get("money").toString());
                //System.out.println(UUID.randomUUID() + ":" + playerName + ":" + email + ":" + password + ":" + money);
                return new Player(UUID.randomUUID(), playerName, email, password, money);
            }
        }
        return null;
    }

    /**
    /*
    public static void createFaction(String factionName,String Description,Boolean opened,int level, int exp, int damages, String recruitLink,
                                    ArrayList<Member> members,ArrayList<UUID> invitations, ArrayList<DimensionalPosition> chunks,
                                    DimensionalBlockPos homePos, ArrayList<Grade> grades, FactionInventory inventory){
        BasicDBObject createFaction = new BasicDBObject();
        createFaction.put("faction", factionName);
        createFaction.put("Description", Description);
        createFaction.put("opened", opened);
        createFaction.put("level", level);
        createFaction.put("exp", exp);
        createFaction.put("damages", damages);
        createFaction.put("members", members);
        createFaction.put("invitations", invitations);
        createFaction.put("chunks", chunks);
        createFaction.put("homePos", homePos);
        createFaction.put("grades", grades);
        createFaction.put("inventory", inventory);

        factionCollection.insert(createFaction);
    }*/

    public static void DeleteFaction(String factionName){
        connection();
        factionCollection.deleteOne(eq("faction", factionName));
    }
}
