package fr.azerks.utils;

import fr.azerks.init.ItemsInit;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ShopPrices {

    private static List<ItemInShop> itemInShops = new ArrayList<ItemInShop>();

    private static void addItemToShop(int ID, ItemStack itemStack, float buyPrice, float sellPrice, float buyQuantity,float sellQuantity){
        itemInShops.add(new ItemInShop(ID, itemStack, buyPrice, sellPrice, buyQuantity, sellQuantity));
    }

    public static void registerItemsPrice(){
        addItemToShop(0, new ItemStack(ItemsInit.aeronium, 1), 1000, 200, 1, 1);
        addItemToShop(1, new ItemStack(ItemsInit.xamos, 1), 200, 50, 1, 1);
        addItemToShop(2, new ItemStack(ItemsInit.zylium, 1), 100, 20, 1, 1);
        addItemToShop(3, new ItemStack(ItemsInit.trinitium, 1), 500, 100, 1, 1);
        addItemToShop(4, new ItemStack(ItemsInit.ocb_paper, 1), 80, 10, 64, 64);
        addItemToShop(5, new ItemStack(ItemsInit.xamos_axe, 1), 500, 100, 64, 64);
        addItemToShop(6, new ItemStack(ItemsInit.zylium_boots, 1), 500, 100, 64, 64);
        addItemToShop(7, new ItemStack(ItemsInit.zylium_leggings, 1), 500, 100, 64, 64);
        addItemToShop(8, new ItemStack(ItemsInit.zylium_chestplate, 1), 500, 100, 64, 64);
        addItemToShop(9, new ItemStack(ItemsInit.zylium_helmet, 1), 500, 100, 64, 64);
        addItemToShop(10, new ItemStack(ItemsInit.aeronium_pickaxe, 1), 500, 100, 64, 64);
        addItemToShop(11, new ItemStack(ItemsInit.trinitium_sword, 1), 500, 100, 64, 64);
    }

    public static List<ItemInShop> getItemInShops() {
        return itemInShops;
    }

    public static ItemInShop getItemInShopsByID(int id){
        for(ItemInShop itemInShop : itemInShops){
            if (itemInShop.getId() == id){
                return itemInShop;
            }
        }
        return null;
    }

    public static class ItemInShop{
        int id;
        ItemStack itemStack;
        float buyPrice;
        float sellPrice;
        float buyQuantity;
        float sellQuantity;

        private ItemInShop(int id, ItemStack itemStack, float buyPrice, float sellPrice, float buyQuantity, float sellQuantity) {
            this.id = id;
            this.itemStack = itemStack;
            this.buyPrice = buyPrice;
            this.sellPrice = sellPrice;
            this.buyQuantity = buyQuantity;
            this.sellQuantity = sellQuantity;
        }

        public ItemStack getItemStack() {
            return itemStack;
        }

        public void setItemStack(ItemStack itemStack) {
            this.itemStack = itemStack;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public float getBuyPrice() {
            return buyPrice;
        }

        public void setBuyPrice(int buyPrice) {
            this.buyPrice = buyPrice;
        }

        public float getSellPrice() {
            return sellPrice;
        }

        public void setSellPrice(int sellPrice) {
            this.sellPrice = sellPrice;
        }

        public float getBuyQuantity() {
            return buyQuantity;
        }

        public void setBuyQuantity(int buyQuantity) {
            this.buyQuantity = buyQuantity;
        }

        public float getSellQuantity() {
            return sellQuantity;
        }

        public void setSellQuantity(int sellQuantity) {
            this.sellQuantity = sellQuantity;
        }
    }
}
