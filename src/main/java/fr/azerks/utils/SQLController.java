package fr.azerks.utils;

import java.sql.*;
import java.util.UUID;

public class SQLController {

    private static Connection sConnection;
    private String urlbase, host, database, user, pass;

    public SQLController(String urlbase, String host, String database, String user, String pass) {
        this.urlbase = urlbase;
        this.host = host;
        this.database = database;
        this.user = user;
        this.pass = pass;
    }

    public void connection() {
        if (!isConnected()) {

            try {
                sConnection = DriverManager.getConnection(urlbase + host + "/" + database, user, pass);
                System.out.println("Succesfuly connected to your database");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void disconnect() {
        if (isConnected()) {
            try {
                sConnection.close();
                System.out.println("Succesfuly disconnected to your database");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isConnected() {
        return sConnection != null;
    }

    // TODO: 24/09/2019 The account is create on the site or by the launcher, This should be never used : used for debug account AzerKs in DedicatedServerEvent
    public void createAccount(Player player) {
        if (!hasAccount(player)) {
            try {
                PreparedStatement q = sConnection.prepareStatement("INSERT INTO player(uuid,username,email,password,money) VALUES (?,?,?,?,?)");
                q.setString(1, player.getUUIDToString());
                q.setString(2, player.getUsername());
                q.setString(3, player.getEmail());
                q.setString(4, player.getPassword());
                q.setFloat(5, player.getMoney());
                q.execute();
                q.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    public Player getPlayer(String player) throws SQLException {
        PreparedStatement q = sConnection.prepareStatement("SELECT * FROM player WHERE username = ?");
        q.setString(1, player);
        ResultSet rs = q.executeQuery();
        String username = "";
        String email = "";
        String password = "";
        float money = 0;

        String uuid = "";
        // TODO: 24/09/2019 UUID ADD ON PLAYER CONNECTION
        while (rs.next()){
            //String uuid = rs.getString("UUDI");
            username = rs.getString(3);
            email = rs.getString(4);
            password = rs.getString(5);
            money = rs.getFloat(6);
            //String faction = rs.getString("faction");
            System.out.println(rs.next());
        }
        rs.close();
        if (uuid.equalsIgnoreCase("")) {
            return new Player(UUID.randomUUID(), username, email, password, money);
        } else {
            return new Player(UUID.fromString(uuid), username, email, password, money);
        }
    }

    public void insert(UUID uuid) {
        try {
            PreparedStatement q = sConnection.prepareStatement("INSERT INTO player(uuid) VALUES (?)");
            q.setString(1, uuid.toString());
            q.execute();
            q.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void update(float money) {
        try {
            PreparedStatement q = sConnection.prepareStatement("UPDATE player SET money = money + ?");
            q.setFloat(1, money);
            q.execute();
            q.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // TODO: 24/09/2019 Insert FACTION

    private boolean hasAccount(Player player) {
        try {
            PreparedStatement q = sConnection.prepareStatement("SELECT username FROM player WHERE username = ?");
            q.setString(1, player.getUsername());
            ResultSet result = q.executeQuery();
            boolean hasAccount = result.next();
            q.close();
            return hasAccount;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean connectionAccount(String username, String pass) {
        try {
            PreparedStatement q = sConnection.prepareStatement("SELECT username, email, password FROM player WHERE username = ? AND password = ?");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Connection getConnection() {
        return sConnection;
    }
}
