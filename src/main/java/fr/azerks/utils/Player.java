package fr.azerks.utils;

import java.util.UUID;

public class Player {
        private UUID uuid;
        private String username;
        private String email;
        private String password;
        private float money;
        private String faction;

        public Player(UUID uuid, String username, String email, String password, float money) {
            this.uuid = uuid;
            this.username = username;
            this.email = email;
            this.password = password;
            this.money = money;
        }

        public UUID getUUID() {
            return uuid;
        }

        public String getUUIDToString() {
            return uuid.toString();
        }

        public String getUsername() {
            return username;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }

        public float getMoney() {
            return money;
        }

        public String getFaction() {
            return faction;
        }
}
