package fr.azerks.proxy;

import fr.azerks.ModTest;
import fr.azerks.World.WorldRegisterAeros;
import fr.azerks.gui.GuiHandler;
import fr.azerks.init.BlocksInit;
import fr.azerks.init.ItemsInit;
import net.minecraftforge.fml.common.network.NetworkRegistry;

import java.io.File;

public class CommonProxy {
    public void preInit(File configFile){
        System.out.println("pre init common");

        /** ITEMS - Renders Items in client proxy*/
        ItemsInit.init();
        ItemsInit.registerItems();

        /** BLOCKS */
        BlocksInit.init();
        BlocksInit.registersBlocks();

        WorldRegisterAeros.MainRegistry();

    }

    public void init(){
        NetworkRegistry.INSTANCE.registerGuiHandler(ModTest.instance, new GuiHandler());
    }
}
