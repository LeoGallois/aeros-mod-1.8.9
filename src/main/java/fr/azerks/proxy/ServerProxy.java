package fr.azerks.proxy;

import fr.azerks.ModTest;
import fr.azerks.init.ItemsInit;
import fr.azerks.utils.DedicatedServerEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;

import java.io.File;

public class ServerProxy extends CommonProxy {
    @Override
    public void preInit(File configFile) {
        super.preInit(configFile);
        System.out.println("Pre init Server");
        new DedicatedServerEvent();
    }

    @Override
    public void init() {
        super.init();
    }
}
