package fr.azerks.proxy;

import fr.azerks.init.BlocksInit;
import fr.azerks.init.ItemsInit;

import java.io.File;

public class ClientProxy extends CommonProxy {
    @Override
    public void preInit(File configFile) {
        super.preInit(configFile);
        System.out.println("pre init client");
        ItemsInit.registerRenders();
    }


    @Override
    public void init() {
        BlocksInit.registerRenders();
        super.init();

    }
}
